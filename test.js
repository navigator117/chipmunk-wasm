const chipmunk = require('./lib/chipmunk').chipmunk;
const libchipmunk = require('./lib/chipmunk').webassembly;
const cpvlib = require('./lib/chipmunk').cpvlib;
const cpslib = require('./lib/chipmunk').cpslib;

libchipmunk().then(function(runtime) {
    chipmunk.reset(runtime);
    
    var space = chipmunk.cpSpaceNew();
    if (space) {
        var data1 = {
            a: "a",
            b: "2"
        };
        chipmunk.cpSpaceSetUserData(space, data1);
        var data2 =  chipmunk.cpSpaceGetUserData(space);
        console.log("datas: ", data1, data2);    
        chipmunk.cpSpaceSetGravity(space, new cpVect(1.0, 2.0));
        var body = chipmunk.cpBodyNew(1.0, 1.0);                    
        if (body) {
            chipmunk.cpSpaceAddBody(space, body);

            var position1 = new cpVect(1.0, 2.0);
            chipmunk.cpBodySetPosition(body, position1);
            var position2 = chipmunk.cpBodyGetPosition(body);
            console.log("positions: ", position1, position2);

            var velocity1 = new cpVect(1.0, 2.0);
            chipmunk.cpBodySetVelocity(body, velocity1);
            var velocity2 = chipmunk.cpBodyGetVelocity(body);
            console.log("velocities: ", velocity1, velocity2);


            var data1 = { name: 'a', size: 2 };
            chipmunk.cpBodySetUserData(body, data1);
            var data2 = chipmunk.cpBodyGetUserData(body);
            console.log("user datas: ", data1, data2);

            var mass1 = 0.5;
            chipmunk.cpBodySetMass(body, mass1);
            var mass2 = chipmunk.cpBodyGetMass(body);
            console.log("body masses: ", mass1, mass2);

            var angle1 = 0.6;
            chipmunk.cpBodySetAngle(body, angle1);
            var angle2 = chipmunk.cpBodyGetAngle(body);
            console.log("body angles: ", angle1, angle2);

            var angularVelocity1 = 0.7;
            chipmunk.cpBodySetAngularVelocity(body, angularVelocity1);
            var angularVelocity2 = chipmunk.cpBodyGetAngularVelocity(body);
            console.log("body angularVelocities: ", angularVelocity1, angularVelocity2);

            var type1 = chipmunk.CP_BODY_TYPE_DYNAMIC;
            chipmunk.cpBodySetType(body, type1);
            var type2 = chipmunk.cpBodyGetType(body);

            console.log("body types: ", type1, type2);

            var shape = chipmunk.cpSegmentShapeNew(body, new cpVect(0.0, 0.0), new cpVect(1.0, 1.0), 1.0);
            if (shape) {
                chipmunk.cpSpaceAddShape(space, shape);
                var filter1 = new cpShapeFilter(cpslib.CP_NO_GROUP, 0x2, 0x4);
                chipmunk.cpShapeSetFilter(shape, filter1);
                var filter2 = chipmunk.cpShapeGetFilter(shape);
                console.log("shape filters: ", filter1, filter2);
                
                var collisionType1 = 23;
                chipmunk.cpShapeSetCollisionType(shape, collisionType1);
                var collisionType2 = chipmunk.cpShapeGetCollisionType(shape);
                console.log("collision types:", collisionType1, collisionType2);

                var elasticity1 = 0.8;
                chipmunk.cpShapeSetElasticity(shape, elasticity1);
                var elasticity2 = chipmunk.cpShapeGetElasticity(shape);
                console.log("elasticity types:", elasticity1, elasticity2);

                var friction1 = 0.7;
                chipmunk.cpShapeSetFriction(shape, friction1);
                var friction2 = chipmunk.cpShapeGetFriction(shape);
                console.log("friction types:", friction1, friction2);

                var sensor1 = true;
                chipmunk.cpShapeSetSensor(shape, sensor1);
                var sensor2 = chipmunk.cpShapeGetSensor(shape);
                console.log("sensor types:", sensor1, sensor2);

                var space2 = chipmunk.cpShapeGetSpace(shape);
                console.log("space: ", space2);

                chipmunk.cpSpaceRemoveShape(space, shape);
                var body1 = body;
                chipmunk.cpShapeSetBody(shape, body1);
                var body2 = chipmunk.cpShapeGetBody(shape);
                console.log("bodies: ", body1, body2);
                chipmunk.cpSpaceAddShape(space, shape);
                
                chipmunk.cpSpaceRemoveShape(space, shape);
                chipmunk.cpShapeFree(shape);            
            }
            else {
                console.log("create shape failed!");            
            }
            var circleShape = chipmunk.cpCircleShapeNew(body, 0.7, new cpVect(1.0, 1.0));
            if (circleShape) {
                console.log("circleShape radius: ", chipmunk.cpCircleShapeGetRadius(circleShape));
                console.log("circleShape offset: ", chipmunk.cpCircleShapeGetOffset(circleShape));

                var radius1 = 0.7;
                chipmunk.cpCircleShapeSetRadius(circleShape, radius1);
                var radius2 = chipmunk.cpCircleShapeGetRadius(circleShape);
                console.log("radiuses:", radius1, radius2);

                var offset1 = new cpVect(1.0, 2.0);
                chipmunk.cpCircleShapeSetOffset(circleShape, offset1);
                var offset2 = chipmunk.cpCircleShapeGetOffset(circleShape);
                console.log("offsets: ", offset1, offset2);

                chipmunk.cpShapeFree(circleShape);
            }
            else {
                console.log("create circleShape failed!");
            }
            var vertexs = [
                new cpVect(-1.0, 1.0),
                new cpVect(1.0, 1.0),
                new cpVect(1.0, -1.0),
                new cpVect(-1.0, -1.0)
            ];
            var polyShape = chipmunk.cpPolyShapeNewRaw(body, vertexs, 0.5);
            if (polyShape) {
                chipmunk.cpShapeFree(polyShape);
            }
            else {
                console.log("create polyShape failed!");
            }
            chipmunk.cpSpaceReindexShapesForBody(space, body);
            var area = chipmunk.cpAreaForCircle(1.0, 0.0);
            console.log("area for circle: ", area);

            var body1 = chipmunk.cpBodyNew(1.0, 1.0);
            chipmunk.cpBodySetPosition(body1, new cpVect(1.0, 1.0));

            var body2 = chipmunk.cpBodyNew(2.0, 2.0);
            chipmunk.cpBodySetPosition(body2, new cpVect(2.0, 2.0));
            
            var constraint = chipmunk.cpSlideJointNew(body1, body2, cpvlib.cpvzero, cpvlib.cpvzero, 0, 1);
            if (constraint) {
                var data1 = {
                    a: "a",
                    b: 1.2
                };
                chipmunk.cpConstraintSetUserData(constraint, data1);
                var data2 = chipmunk.cpConstraintGetUserData(constraint);
                console.log("datas: ", data1, data2);
                chipmunk.cpSpaceAddConstraint(space, constraint);
                var maxBias1 = 0.2;
                chipmunk.cpConstraintSetMaxBias(constraint, maxBias1);
                var maxBias2 = chipmunk.cpConstraintGetMaxBias(constraint);
                console.log("maxBiases: ", maxBias1, maxBias2);

                var maxForce1 = 0.3;
                chipmunk.cpConstraintSetMaxBias(constraint, maxForce1);
                var maxForce2 = chipmunk.cpConstraintGetMaxBias(constraint);
                console.log("maxForcees: ", maxForce1, maxForce2);

                var errorBias1 = 0.4;
                chipmunk.cpConstraintSetMaxBias(constraint, errorBias1);
                var errorBias2 = chipmunk.cpConstraintGetMaxBias(constraint);
                console.log("errorBiases: ", errorBias1, errorBias2);            
                
                chipmunk.cpSpaceRemoveConstraint(space, constraint);
                chipmunk.cpConstraintFree(constraint);
            }
            else {
                console.log("create cpSlideJoint failed!");
            }
            var constraint2 = chipmunk.cpPivotJointNew2(body1, body2, cpvlib.cpvzero, cpvlib.cpvzero);
            if (constraint2) {
                chipmunk.cpConstraintFree(constraint);
            }
            else {
                console.log("create cpPivotJointNew2 failed!");            
            }
            
            chipmunk.cpBodyFree(body1);
            chipmunk.cpBodyFree(body2);        
            
            chipmunk.cpBodySetVelocityUpdateFunc(body, function(body, gravity, damping, dt) {
                console.log("update velocity ...", body, gravity, damping, dt);
                chipmunk.cpBodyUpdateVelocity(body, gravity, damping, dt);
            }.bind(this));

            chipmunk.cpSpaceSetDebugDrawMode(space, chipmunk.DebugDrawModes.DBG_DrawShapes);
            chipmunk.cpSpaceSetDebugDrawer(space, {
                drawLine: function(a, b, color) { console.log('drawLine', a, b, color); },
                drawUpdate: function() { console.log('drawUpdate'); }
            });

            chipmunk.cpSpaceAddCollisionHandler(space,
                                                cpslib.CP_WILDCARD_COLLISION_TYPE,
                                                cpslib.CP_WILDCARD_COLLISION_TYPE, {
                                                    beginFunc: function(arbiter, space, data) {
                                                        console.log("beginFunc ...");
                                                        chipmunk.cpSpaceAddPostStepCallback(space,
                                                                                            function() {
                                                                                                console.log("postStepCallback");
                                                                                            },
                                                                                            1,
                                                                                            null);
                                                    },
                                                    preSolveFunc: function() {
                                                        console.log("preSolveFunc ...");
                                                    },
                                                    postSolveFunc: function() {
                                                        console.log("postSolveFunc ...");
                                                    },
                                                    separateFunc: function() {
                                                        console.log("separateFunc ...");                                                    
                                                    }
                                                });
            
            setInterval(function() {
                console.log("step ...");
                chipmunk.cpSpaceStep(space, 0.1);
                chipmunk.cpSpaceDebugDraw(space);
            }.bind(this), 100);
            setTimeout(function() {
                chipmunk.cpSpaceRemoveBody(space, body);
                chipmunk.cpBodyFree(body);
                chipmunk.cpSpaceFree(space);
            }, 100000);
        }
        else {
            console.log("create body failed!");
        }
    }
    else {
        console.log("create space failed!");
    }
});
