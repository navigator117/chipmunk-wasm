var libchipmunk = null;
if (typeof global !== 'undefined') {
    libchipmunk = require('../src/libchipmunk.node');
}
else if (typeof window !== 'undefined') {
    libchipmunk = require('../src/libchipmunk.web');
}
else if (typeof self !== 'undefined') {
    libchipmunk = require('../src/libchipmunk.worker');
}
else if (typeof GameGlobal !== 'undefined' || typeof worker !== 'undefined') {
    libchipmunk = require('../src/libchipmunk.wechat');
}
var cpvlib = require('./cpVect');
var cpslib = require('./cpShape');

const PROP_NAME_VELOCITY_UPDATE_FUNC = ("__VELOCITY_UPDATE_FUNC__");
const PROP_NAME_VELOCITY_UPDATE_THIS = ("__VELOCITY_UPDATE_THIS__");
const PROP_NAME_POSITION_UPDATE_FUNC = ("__POSITION_UPDATE_FUNC__");
const PROP_NAME_POSITION_UPDATE_THIS = ("__POSITION_UPDATE_THIS__");
const PROP_NAME_POST_STEP_CALLBACK_FUNC = ("__POST_STEP_CALLBACK_FUNC__");
const PROP_NAME_SPACE_POINT_QUERY_FUNC = ("__SPACE_POINT_QUERY_FUNC__");
const PROP_NAME_SPACE_SHAPE_QUERY_FUNC = ("__SPACE_SHAPE_QUERY_FUNC__");

var Chipmunk = function()
{
    this.runtime = null;
    this.spaceUserData = null;
    this.bodiesUserDatas = new Map();
    this.postStepsUserDatas = new Map();
    this.collisionUserDatas = new Map();
    this.pointQueryUserData = null;
    this.shapeQueryUserData = null;
    this.constraintsUserDatas = new Map();
    this.spaceDebugDrawers = new Map();
    this.spaceDebugDrawFlags = new Map();
}

Chipmunk.prototype.reset = function(runtime)
{
    this.runtime = runtime;
    this.cpBodyUpdateVelocityFuncPtr = runtime.addFunction(this.cpBodyUpdateVelocityFunc.bind(this), 'vddddd');
    this.cpBodyUpdatePositionDefaultFuncPtr = runtime.addFunction(this.cpBodyUpdatePositionDefaultFunc.bind(this), 'vdd');
    this.cpBodyUpdatePositionCurrentFuncPtr = runtime.addFunction(this.cpBodyUpdatePositionCurrentFunc.bind(this), 'vdd');
    this.cpSpacePostStepCallbackFuncPtr = runtime.addFunction(this.cpSpacePostStepCallbackFunc.bind(this), 'vddd');
    this.cpSpaceCollisonBeginFuncPtr = runtime.addFunction(this.cpSpaceCollisonBeginFunc.bind(this), 'iddd');
    this.cpSpaceCollisonPreSolveFuncPtr = runtime.addFunction(this.cpSpaceCollisonPreSolveFunc.bind(this), 'iddd');
    this.cpSpaceCollisonPostSolveFuncPtr = runtime.addFunction(this.cpSpaceCollisonPostSolveFunc.bind(this), 'vddd');
    this.cpSpaceCollisonSeparateFuncPtr = runtime.addFunction(this.cpSpaceCollisonSeparateFunc.bind(this), 'vddd');    
    this.cpSpacePointQueryFuncPtr = runtime.addFunction(this.cpSpacePointQueryFunc.bind(this), 'vdddddd');
    this.cpSpaceShapeQueryFuncPtr = runtime.addFunction(this.cpSpaceShapeQueryFunc.bind(this), 'vdd');    
    this.cpSpaceDebugDrawSegmentFuncPtr = runtime.addFunction(this.cpSpaceDebugDrawSegmentFunc.bind(this), 'vdddd');
}

Chipmunk.prototype.cpBodyNew = function(mass, moment)
{
    return this.runtime.ccall('cpBodyNew', 'number', ['number', 'number'], [mass, moment]);
}

Chipmunk.prototype.cpBodyNewStatic = function()
{
    return this.runtime.ccall('cpBodyNewStatic', 'number');
}

Chipmunk.prototype.cpBodyNewKinematic = function()
{
    return this.runtime.ccall('cpBodyNewKinematic', 'number');
}

Chipmunk.prototype.cpBodyFree = function(body)
{
    this.runtime.ccall('cpBodyFree', null, ['number'], [body]);
}

Chipmunk.prototype.cpBodySetUserData = function(body, userData)
{
    this.bodiesUserDatas.set(body, userData);
}

Chipmunk.prototype.cpBodyGetUserData = function(body)
{
    return this.bodiesUserDatas.get(body);
}

Chipmunk.prototype.cpBodyGetPosition = function(body)
{
    var position = cpvlib.cpv(0.0, 0.0);
    var stack = this.runtime.stackSave();
    var positionPtr = this.runtime.allocate([position.x, position.y], 'double', this.runtime.ALLOC_STACK);
    this.runtime.ccall('chipmunk_cpBodyGetPosition', null, ['number', 'number'], [body, positionPtr]);
    position.x = this.runtime.getValue(positionPtr + 0, 'double');
    position.y = this.runtime.getValue(positionPtr + 8, 'double');
    this.runtime.stackRestore(stack);
    return position;
}

Chipmunk.prototype.cpBodySetPosition = function(body, position)
{
    this.runtime.ccall('chipmunk_cpBodySetPosition', null, ['number', 'number', 'number'], [body, position.x, position.y]);
}

Chipmunk.prototype.cpBodyGetVelocity = function(body)
{
    var velocity = cpvlib.cpv(0.0, 0.0);
    var stack = this.runtime.stackSave();
    var velocityPtr = this.runtime.allocate([velocity.x, velocity.y], 'double', this.runtime.ALLOC_STACK);
    this.runtime.ccall('chipmunk_cpBodyGetVelocity', null, ['number', 'number'], [body, velocityPtr]);
    velocity.x = this.runtime.getValue(velocityPtr + 0, 'double');
    velocity.y = this.runtime.getValue(velocityPtr + 8, 'double');
    this.runtime.stackRestore(stack);
    return velocity;
}

Chipmunk.prototype.cpBodySetVelocity = function(body, velocity)
{
    this.runtime.ccall('chipmunk_cpBodySetVelocity', null, ['number', 'number', 'number'], [body, velocity.x, velocity.y]);
}

Chipmunk.prototype.cpBodyGetMass = function(body)
{
    return this.runtime.ccall('cpBodyGetMass', 'number', ['number'], [body]);
}

Chipmunk.prototype.cpBodySetMass = function(body, mass)
{
    this.runtime.ccall('cpBodySetMass', null, ['number', 'number'], [body, mass]);
}

Chipmunk.prototype.cpBodyGetAngle = function(body)
{
    return this.runtime.ccall('cpBodyGetAngle', 'number', ['number'], [body]);
}

Chipmunk.prototype.cpBodySetAngle = function(body, angle)
{
    this.runtime.ccall('cpBodySetAngle', null, ['number', 'number'], [body, angle]);
}

Chipmunk.prototype.cpBodyGetAngularVelocity = function(body)
{
    return this.runtime.ccall('cpBodyGetAngularVelocity', 'number', ['number'], [body]);
}

Chipmunk.prototype.cpBodySetAngularVelocity = function(body, angularVelocity)
{
    this.runtime.ccall('cpBodySetAngularVelocity', null, ['number', 'number'], [body, angularVelocity]);
}

Chipmunk.prototype.cpBodyGetType = function(body)
{
    return this.runtime.ccall('cpBodyGetType', 'number', ['number'], [body]);
}

Chipmunk.prototype.cpBodySetType = function(body, type)
{
    this.runtime.ccall('cpBodySetType', null, ['number', 'number'], [body, type]);
}

Chipmunk.prototype.cpBodyIsSleeping = function(body)
{
    return this.runtime.ccall('cpBodyIsSleeping', 'boolean', ['number'], [body]);
}

Chipmunk.prototype.cpBodyActivate = function(body)
{
    return this.runtime.ccall('cpBodyActivate', null, ['number'], [body]);
}

Chipmunk.prototype.cpBodySleep = function(body)
{
    return this.runtime.ccall('cpBodySleep', null, ['number'], [body]);
}

Chipmunk.prototype.cpBodyUpdateVelocity = function(body, gravity, damping, dt)
{
    return this.runtime.ccall('chipmunk_cpBodyUpdateVelocity',
                              null, ['number', 'number', 'number', 'number', 'number'], [body, gravity.x, gravity.y, damping, dt]);
}

Chipmunk.prototype.cpBodyUpdateVelocityFunc = function(body, gravityX, gravityY, damping, dt)
{
    var userData = this.cpBodyGetUserData(body);
    if (userData) {
        var velocityUpdateFunc = userData[PROP_NAME_VELOCITY_UPDATE_FUNC];
        var velocityUpdateThis = userData[PROP_NAME_VELOCITY_UPDATE_THIS];
        velocityUpdateFunc.call(velocityUpdateThis, body, cpvlib.cpv(gravityX, gravityY), damping, dt);
    }
    else {
        throw new Error("invalid user data!");
    }
}

Chipmunk.prototype.cpBodySetVelocityUpdateFunc = function(body, velocityUpdateFunc, velocityUpdateThis)
{
    if (velocityUpdateFunc != null) {
        var userData = this.cpBodyGetUserData(body);
        if (userData) {
            userData[PROP_NAME_VELOCITY_UPDATE_FUNC] = velocityUpdateFunc;
            userData[PROP_NAME_VELOCITY_UPDATE_THIS] = velocityUpdateThis;
            return this.runtime.ccall('chipmunk_cpBodySetVelocityUpdateFunc', null, ['number', 'number'], [body, this.cpBodyUpdateVelocityFuncPtr]);
        }
        else {
            throw new Error("invalid user data!");
        }
    }
    else {
        return this.runtime.ccall('chipmunk_cpBodySetVelocityUpdateFunc', null, ['number', 'number'], [body, 0]);
    }
}

Chipmunk.prototype.cpBodyUpdatePosition = function(body, dt)
{
    return this.runtime.ccall('cpBodyUpdatePosition', null, ['number', 'number'], [body, dt]);
}

Chipmunk.prototype.cpBodyUpdatePositionDefaultFunc = function(body, dt)
{
    return this.runtime.ccall('cpBodyUpdatePosition', null, ['number', 'number'], [body, dt]);
}

Chipmunk.prototype.cpBodyUpdatePositionCurrentFunc = function(body, dt)
{
    var userData = this.cpBodyGetUserData(body);
    if (userData) {
        var positionUpdateFunc = userData[PROP_NAME_POSITION_UPDATE_FUNC];
        var positionUpdateThis = userData[PROP_NAME_POSITION_UPDATE_THIS];
        positionUpdateFunc.call(positionUpdateThis, body, dt);
    }
    else {
        throw new Error("invalid user data!");
    }
}

Chipmunk.prototype.cpBodySetPositionUpdateFunc = function(body, positionUpdateFunc, positionUpdateThis)
{
    if (positionUpdateFunc != null) {
        var userData = this.cpBodyGetUserData(body);
        if (userData) {
            userData[PROP_NAME_POSITION_UPDATE_FUNC] = positionUpdateFunc;
            userData[PROP_NAME_POSITION_UPDATE_THIS] = positionUpdateThis;
            return this.runtime.ccall('cpBodySetPositionUpdateFunc', null, ['number', 'number'], [body, this.cpBodyUpdatePositionCurrentFuncPtr]);
        }
        else {
            throw new Error("invalid user data!");
        }
    }
    else {
        return this.runtime.ccall('cpBodySetPositionUpdateFunc', null, ['number', 'number'], [body, this.cpBodyUpdatePositionDefaultFuncPtr]);
    }
}

Chipmunk.prototype.cpBodyApplyForceAtWorldPoint = function(body, force, point)
{
    return this.runtime.ccall('chipmunk_cpBodyApplyForceAtWorldPoint', null, ['number', 'number', 'number', 'number', 'number'], [body, force.x, force.y, point.x, point.y]);
}

Chipmunk.prototype.cpBodyApplyForceAtLocalPoint = function(body, force, point)
{
    return this.runtime.ccall('chipmunk_cpBodyApplyForceAtLocalPoint', null, ['number', 'number', 'number', 'number', 'number'], [body, force.x, force.y, point.x, point.y]);
}

Chipmunk.prototype.cpBodyApplyImpulseAtWorldPoint = function(body, impulse, point)
{
    return this.runtime.ccall('chipmunk_cpBodyApplyImpulseAtWorldPoint', null, ['number', 'number', 'number', 'number', 'number'], [body, impulse.x, impulse.y, point.x, point.y]);
}

Chipmunk.prototype.cpBodyApplyImpulseAtLocalPoint = function(body, impulse, point)
{
    return this.runtime.ccall('chipmunk_cpBodyApplyImpulseAtLocalPoint', null, ['number', 'number', 'number', 'number', 'number'], [body, impulse.x, impulse.y, point.x, point.y]);
}

Chipmunk.prototype.cpSpaceNew = function()
{
    return this.runtime.ccall('cpSpaceNew', 'number');
}

Chipmunk.prototype.cpSpaceContainsBody = function(space, body)
{
    return this.runtime.ccall('cpSpaceContainsBody', 'boolean', ['number', 'number'], [space, body]);
}

Chipmunk.prototype.cpSpaceContainsShape = function(space, shape)
{
    return this.runtime.ccall('cpSpaceContainsShape', 'boolean', ['number', 'number'], [space, shape]);
}

Chipmunk.prototype.cpSpaceAddBody = function(space, body)
{
    return this.runtime.ccall('cpSpaceAddBody', null, ['number', 'number'], [space, body]);
}

Chipmunk.prototype.cpSpaceRemoveBody = function(space, body)
{
    return this.runtime.ccall('cpSpaceRemoveBody', null, ['number', 'number'], [space, body]);
}

Chipmunk.prototype.cpSpaceReindexShapesForBody = function(space, body)
{
    return this.runtime.ccall('cpSpaceReindexShapesForBody', null, ['number', 'number'], [space, body]);
}

Chipmunk.prototype.cpSpaceAddShape = function(space, shape)
{
    return this.runtime.ccall('cpSpaceAddShape', null, ['number', 'number'], [space, shape]);
}

Chipmunk.prototype.cpSpaceRemoveShape = function(space, shape)
{
    return this.runtime.ccall('cpSpaceRemoveShape', null, ['number', 'number'], [space, shape]);
}

Chipmunk.prototype.cpSpaceAddConstraint = function(space, constraint)
{
    return this.runtime.ccall('cpSpaceAddConstraint', null, ['number', 'number'], [space, constraint]);
}

Chipmunk.prototype.cpSpaceRemoveConstraint = function(space, constraint)
{
    return this.runtime.ccall('cpSpaceRemoveConstraint', null, ['number', 'number'], [space, constraint]);
}

Chipmunk.prototype.cpSpaceStep = function(space, dt)
{
    this.runtime.ccall('cpSpaceStep', null, ['number', 'number'], [space, dt]);
}

Chipmunk.prototype.cpSpaceSetIterations = function(space, iterations)
{
    this.runtime.ccall('cpSpaceSetIterations', null, ['number', 'number'], [space, iterations]);
}

Chipmunk.prototype.cpSpaceGetIterations = function(space)
{
    return this.runtime.ccall('cpSpaceGetIterations', 'number', ['number'], [space]);
}

Chipmunk.prototype.cpSpaceSetIdleSpeedThreshold = function(space, idleSpeedThreshold)
{
    this.runtime.ccall('cpSpaceSetIdleSpeedThreshold', null, ['number', 'number'], [space, idleSpeedThreshold]);
}

Chipmunk.prototype.cpSpaceGetIdleSpeedThreshold = function(space)
{
    return this.runtime.ccall('cpSpaceGetIdleSpeedThreshold', 'number', ['number'], [space]);
}

Chipmunk.prototype.cpSpaceSetSleepTimeThreshold = function(space, sleepTimeThreshold)
{
    this.runtime.ccall('cpSpaceSetSleepTimeThreshold', null, ['number', 'number'], [space, sleepTimeThreshold]);
}

Chipmunk.prototype.cpSpaceGetSleepTimeThreshold = function(space)
{
    return this.runtime.ccall('cpSpaceGetSleepTimeThreshold', 'number', ['number'], [space]);
}

Chipmunk.prototype.cpSpaceSetCollisionSlop = function(space, collisionSlop)
{
    this.runtime.ccall('cpSpaceSetCollisionSlop', null, ['number', 'number'], [space, collisionSlop]);    
}

Chipmunk.prototype.cpSpaceGetCollisionSlop = function(space)
{
    return this.runtime.ccall('cpSpaceGetCollisionSlop', 'number', ['number'], [space]);
}

Chipmunk.prototype.cpSpaceSetCollisionBias = function(space, collisionBias)
{
    this.runtime.ccall('cpSpaceSetCollisionBias', null, ['number', 'number'], [space, collisionBias]);    
}

Chipmunk.prototype.cpSpaceGetCollisionBias = function(space)
{
    return this.runtime.ccall('cpSpaceGetCollisionBias', 'number', ['number'], [space]);
}

Chipmunk.prototype.cpSpaceSetGravity = function(space, gravity)
{
    this.runtime.ccall('chipmunk_cpSpaceSetGravity', null, ['number', 'number', 'number'], [space, gravity.x, gravity.y]);
}

Chipmunk.prototype.cpSpaceGetGravity = function(space)
{
    var gravity = cpvlib.cpv(0.0, 0.0);
    var stack = this.runtime.stackSave();
    var gravityPtr = this.runtime.allocate([gravity.x, gravity.y], 'double', this.runtime.ALLOC_STACK);
    this.runtime.ccall('chipmunk_cpSpaceGetGravity', null, ['number', 'number'], [space, gravityPtr]);
    gravity.x = this.runtime.getValue(gravityPtr + 0, 'double');
    gravity.y = this.runtime.getValue(gravityPtr + 8, 'double');
    this.runtime.stackRestore(stack);
    return gravity;
}

Chipmunk.prototype.cpSpaceGetDamping = function(space)
{
    return this.runtime.ccall('cpSpaceGetDamping', 'number', ['number'], [space]);
}

Chipmunk.prototype.cpSpaceSetDamping = function(space, damping)
{
    this.runtime.ccall('cpSpaceSetDamping', null, ['number', 'number'], [space, damping]);
}

Chipmunk.prototype.cpSpaceSetUserData = function(space, userData)
{
    this.spaceUserData = userData;
}

Chipmunk.prototype.cpSpaceGetUserData = function(space)
{
    return this.spaceUserData;
}

Chipmunk.prototype.cpSpacePostStepCallbackFunc = function(space, body)
{
    var userData = this.postStepsUserDatas.get(body);
    if (userData) {
        var postStepCallbackFunc = userData[PROP_NAME_POST_STEP_CALLBACK_FUNC];
        postStepCallbackFunc(space, userData.key, userData.data);
    }
    else {
        throw new Error("invalid user data!");
    }
}

Chipmunk.prototype.cpSpaceAddPostStepCallback = function(space, callback, key, data)
{
    if (key.body) {
        var userData = {
            key: key,
            data: data
        };
        userData[PROP_NAME_POST_STEP_CALLBACK_FUNC] = callback;
        this.postStepsUserDatas.set(key.body, userData);
        this.runtime.ccall('cpSpaceAddPostStepCallback', null,
                           ['number', 'number', 'number', 'number'],
                           [space, this.cpSpacePostStepCallbackFuncPtr, key.body, key.body]);
    }
    else {
        throw new Error("invalid key without a body!");
    }
}

Chipmunk.prototype.cpSpaceCollisonBeginFunc = function(arbiter, space, collisionKey)
{
    var userData = this.collisionUserDatas.get(collisionKey);
    if (userData) {
        return userData.beginFunc(arbiter, space, userData);
    }
    else {
        throw new Error("invalid collisionKey!");
    }
}

Chipmunk.prototype.cpSpaceCollisonPreSolveFunc = function(arbiter, space, collisionKey)
{
    var userData = this.collisionUserDatas.get(collisionKey);
    if (userData) {
        return userData.preSolveFunc(arbiter, space, userData);
    }
    else {
        throw new Error("invalid collisionKey!");
    }
}

Chipmunk.prototype.cpSpaceCollisonPostSolveFunc = function(arbiter, space, collisionKey)
{
    var userData = this.collisionUserDatas.get(collisionKey);
    if (userData) {
        userData.postSolveFunc(arbiter, space, userData);
    }
    else {
        throw new Error("invalid collisionKey!");
    }
}

Chipmunk.prototype.cpSpaceCollisonSeparateFunc = function(arbiter, space, collisionKey)
{
    var userData = this.collisionUserDatas.get(collisionKey);
    if (userData) {
        userData.separateFunc(arbiter, space, userData);
    }
    else {
        throw new Error("invalid collisionKey!");
    }    
}

Chipmunk.prototype.cpSpaceAddCollisionHandler = function(space, collisionTypeA, collisionTypeB, userData)
{
    if (collisionTypeA > 0xffff ||
        collisionTypeB > 0xffff) {
        throw new Error("invalid collisionType!");
    }
    var collisionKey = collisionTypeA << 16 | collisionTypeB;
    var collisionHandler = this.runtime.ccall('chipmunk_cpSpaceAddCollisionHandler', 'number',
                                              [
                                                  'number', 'number', 'number', 'number',
                                                  'number', 'number', 'number', 'number'
                                              ],
                                              [
                                                  space, collisionTypeA, collisionTypeB, collisionKey,
                                                  (userData.beginFunc ? this.cpSpaceCollisonBeginFuncPtr : 0),
                                                  (userData.preSolveFunc ? this.cpSpaceCollisonPreSolveFuncPtr : 0),
                                                  (userData.postSolveFunc ? this.cpSpaceCollisonPostSolveFuncPtr : 0),
                                                  (userData.separateFunc ? this.cpSpaceCollisonSeparateFuncPtr : 0)
                                              ]);
    if (collisionHandler) {
        this.collisionUserDatas.set(collisionKey, userData);
    }
    else {
        throw new Error("call cpSpaceAddCollisionHandler failed!");
    }
}

Chipmunk.prototype.cpSpacePointQueryFunc = function(shape, pointX, pointY, distance, gradientX, gradientY)
{
    if (this.pointQueryUserData != null) {
        var pointQueryCallback = this.pointQueryUserData[PROP_NAME_SPACE_POINT_QUERY_FUNC];
        if (pointQueryCallback) {
            pointQueryCallback(shape, cpvlib.cpv(pointX, pointY), distance, cpvlib.cpv(gradientX, gradientY), this.pointQueryUserData);
        }
    }
}

Chipmunk.prototype.cpSpacePointQuery = function(space, point, maxDistance, filter, callback, userData)
{
    if (this.pointQueryUserData != null) {
        throw new Error("another pointQuery is on going!");
    }
    this.pointQueryUserData = userData;
    this.pointQueryUserData[PROP_NAME_SPACE_POINT_QUERY_FUNC] = callback;
    this.runtime.ccall('chipmunk_cpSpacePointQuery', null,
                       ['number', 'number', 'number', 'number', 'number', 'number', 'number', 'number'],
                       [space, point.x, point.y, maxDistance, filter.group, filter.categories, filter.mask, this.cpSpacePointQueryFuncPtr]);
    this.pointQueryUserData = null;
}

Chipmunk.prototype.cpSpaceShapeQueryFunc = function(shape, points)
{
    if (this.shapeQueryUserData != null) {
        var shapeQueryCallback = this.shapeQueryUserData[PROP_NAME_SPACE_SHAPE_QUERY_FUNC];
        if (shapeQueryCallback) {
            shapeQueryCallback(shape, points, this.shapeQueryUserData);
        }
    }
}

Chipmunk.prototype.cpSpaceSetDebugDrawMode = function(space, debugDrawMode)
{
    this.spaceDebugDrawFlags.set(space, debugDrawMode);
}

Chipmunk.prototype.cpSpaceGetDebugDrawMode = function(space)
{
    return this.spaceDebugDrawFlags.get(space);
}

Chipmunk.prototype.cpSpaceSetDebugDrawer = function(space, debugDrawer)
{
    this.spaceDebugDrawers.set(space, debugDrawer);
}

Chipmunk.prototype.cpSpaceDebugDrawSegmentFunc = function(space, aX, aY, bX, bY, colorR, colorG, colorB)
{
    var debugDrawer = this.spaceDebugDrawers.get(space);
    debugDrawer && debugDrawer.drawLine({
        x: aX,
        y: aY,
        z: 0
    }, {
        x: bX,
        y: bY,
        z: 0
    }, {
        x: colorR,
        y: colorG,
        z: colorB
    });
}

Chipmunk.prototype.cpSpaceDebugDraw = function(space)
{
    var debugDrawer = this.spaceDebugDrawers.get(space);
    debugDrawer && debugDrawer.drawUpdate();    
    var flags = this.spaceDebugDrawFlags.get(space);
    this.runtime.ccall('chipmunk_cpSpaceDebugDraw', null, ['number', 'number', 'number'], [space, flags, this.cpSpaceDebugDrawSegmentFuncPtr]);
}

Chipmunk.prototype.cpSpaceShapeQuery = function(space, shape, callback, userData)
{
    if (this.shapeQueryUserData != null) {
        throw new Error("another shapeQuery is on going!");
    }
    this.shapeQueryUserData = userData;
    this.shapeQueryUserData[PROP_NAME_SPACE_SHAPE_QUERY_FUNC] = callback;
    this.runtime.ccall('chipmunk_cpSpaceShapeQuery', null, ['number', 'number', 'number'], [space, shape, this.cpSpaceShapeQueryFuncPtr]);
    this.shapeQueryUserData = null;
}

Chipmunk.prototype.cpSpaceFree = function(space)
{
    this.runtime.ccall('cpSpaceFree', null, ['number'], [space]);
}

Chipmunk.prototype.cpSegmentShapeNew = function(body, a, b, radius)
{
    return this.runtime.ccall('chipmunk_cpSegmentShapeNew', 'number',
                              ['number', 'number', 'number', 'number', 'number', 'number'],
                              [body, a.x, a.y, b.x, b.y, radius]);
}

Chipmunk.prototype.cpSegmentShapeGetA = function(shape)
{
    var a = cpvlib.cpv(0.0, 0.0);
    var stack = this.runtime.stackSave();
    var aPtr = this.runtime.allocate([a.x, a.y], 'double', this.runtime.ALLOC_STACK);
    this.runtime.ccall('chipmunk_cpSegmentShapeGetA', null, ['number', 'number'], [shape, aPtr]);
    a.x = this.runtime.getValue(aPtr + 0, 'double');
    a.y = this.runtime.getValue(aPtr + 8, 'double');
    this.runtime.stackRestore(stack);
    return a;
}

Chipmunk.prototype.cpSegmentShapeGetB = function(shape)
{
    var b = cpvlib.cpv(0.0, 0.0);
    var stack = this.runtime.stackSave();
    var bPtr = this.runtime.allocate([b.x, b.y], 'double', this.runtime.ALLOC_STACK);
    this.runtime.ccall('chipmunk_cpSegmentShapeGetB', null, ['number', 'number'], [shape, bPtr]);
    b.x = this.runtime.getValue(bPtr + 0, 'double');
    b.y = this.runtime.getValue(bPtr + 8, 'double');
    this.runtime.stackRestore(stack);
    return b;
}

Chipmunk.prototype.cpCircleShapeNew = function(body, radius, offset)
{
    return this.runtime.ccall('chipmunk_cpCircleShapeNew', 'number', ['number', 'number', 'number', 'number'], [body, radius, offset.x, offset.y]);
}

Chipmunk.prototype.cpPolyShapeNewRaw = function(body, vertexs, radius)
{
    var numOfVertex = vertexs.length;
    var buffer = new Float64Array(numOfVertex * 2);
    for (var i=0; i<numOfVertex; ++i) {
        buffer.set([vertexs[i].x, vertexs[i].y], i * 2);
    }
    return this.runtime.ccall('chipmunk_cpPolyShapeNewRaw', 'number',
                              ['number', 'number', 'array', 'number'],
                              [body, numOfVertex, new Uint8Array(buffer.buffer), radius]);
}

Chipmunk.prototype.cpCircleShapeGetRadius = function(shape)
{
    return this.runtime.ccall('cpCircleShapeGetRadius', 'number', ['number'], [shape]);
}

Chipmunk.prototype.cpCircleShapeSetRadius = function(shape, radius)
{
    return this.runtime.ccall('cpCircleShapeSetRadius', null, ['number', 'number'], [shape, radius]);
}

Chipmunk.prototype.cpCircleShapeGetOffset = function(circleShape)
{
    var offset = cpvlib.cpv(0.0, 0.0);
    var stack = this.runtime.stackSave();
    var offsetPtr = this.runtime.allocate([offset.x, offset.y], 'double', this.runtime.ALLOC_STACK);
    this.runtime.ccall('chipmunk_cpCircleShapeGetOffset', null, ['number', 'number'], [circleShape, offsetPtr]);
    offset.x = this.runtime.getValue(offsetPtr + 0, 'double');
    offset.y = this.runtime.getValue(offsetPtr + 8, 'double');
    this.runtime.stackRestore(stack);
    return offset;
}

Chipmunk.prototype.cpCircleShapeSetOffset = function(circleShape, offset)
{
    this.runtime.ccall('chipmunk_cpCircleShapeSetOffset', null, ['number', 'number', 'number'], [circleShape, offset.x, offset.y]);
}

Chipmunk.prototype.cpShapeGetSpace = function(shape)
{
    return this.runtime.ccall('cpShapeGetSpace', 'number', ['number'], [shape]);
}

Chipmunk.prototype.cpShapeGetBody = function(shape)
{
    return this.runtime.ccall('cpShapeGetBody', 'number', ['number'], [shape]);
}

Chipmunk.prototype.cpShapeSetBody = function(shape, body)
{
    this.runtime.ccall('cpShapeSetBody', null, ['number', 'number'], [shape, body]);
}

Chipmunk.prototype.cpShapeGetFilter = function(shape)
{
    var filter = cpslib.cpShapeFilterNew(cpslib.CP_NO_GROUP, cpslib.CP_ALL_CATEGORIES, cpslib.CP_ALL_CATEGORIES);
    var stack = this.runtime.stackSave();
    var filterPtr = this.runtime.allocate([filter.group, filter.categories, filter.mask], 'i32', this.runtime.ALLOC_STACK);
    this.runtime.ccall('chipmunk_cpShapeGetFilter', null, ['number', 'number'], [shape, filterPtr]);
    filter.group = this.runtime.getValue(filterPtr + 0, 'i32');
    filter.categories = this.runtime.getValue(filterPtr + 4, 'i32');
    filter.mask = this.runtime.getValue(filterPtr + 8, 'i32');
    return filter;
}

Chipmunk.prototype.cpShapeSetFilter = function(shape, filter)
{
    this.runtime.ccall('chipmunk_cpShapeSetFilter', null, ['number', 'number', 'number', 'number'], [shape, filter.group, filter.categories, filter.mask]);
}

Chipmunk.prototype.cpShapeGetCollisionType = function(shape)
{
    return this.runtime.ccall('cpShapeGetCollisionType', 'number', ['number'], [shape]);
}

Chipmunk.prototype.cpShapeSetCollisionType = function(shape, collisionType)
{
    return this.runtime.ccall('cpShapeSetCollisionType', null, ['number', 'number'], [shape, collisionType]);
}

Chipmunk.prototype.cpShapeGetElasticity = function(shape)
{
    return this.runtime.ccall('cpShapeGetElasticity', 'number', ['number'], [shape]);
}

Chipmunk.prototype.cpShapeSetElasticity = function(shape, elasticity)
{
    return this.runtime.ccall('cpShapeSetElasticity', null, ['number', 'number'], [shape, elasticity]);
}

Chipmunk.prototype.cpShapeGetFriction = function(shape)
{
    return this.runtime.ccall('cpShapeGetFriction', 'number', ['number'], [shape]);
}

Chipmunk.prototype.cpShapeSetFriction = function(shape, friction)
{
    return this.runtime.ccall('cpShapeSetFriction', null, ['number', 'number'], [shape, friction]);
}

Chipmunk.prototype.cpShapeGetSensor = function(shape)
{
    return this.runtime.ccall('cpShapeGetSensor', 'boolean', ['number'], [shape]);
}

Chipmunk.prototype.cpShapeSetSensor = function(shape, sensor)
{
    return this.runtime.ccall('cpShapeSetSensor', null, ['number', 'boolean'], [shape, sensor]);
}

Chipmunk.prototype.cpShapeFree = function(shape)
{
    this.runtime.ccall('cpShapeFree', null, ['number'], [shape]);
}

Chipmunk.prototype.cpSlideJointNew = function(bodyA, bodyB, anchorA, anchorB, min, max)
{
    return this.runtime.ccall('chipmunk_cpSlideJointNew', 'number',
                              ['number', 'number', 'number', 'number', 'number', 'number', 'number', 'number'],
                              [bodyA, bodyB, anchorA.x, anchorA.y, anchorB.x, anchorB.y, min, max]);
}

Chipmunk.prototype.cpPivotJointNew2 = function(bodyA, bodyB, anchorA, anchorB)
{
    return this.runtime.ccall('chipmunk_cpPivotJointNew2', 'number',
                              ['number', 'number', 'number', 'number', 'number', 'number'],
                              [bodyA, bodyB, anchorA.x, anchorA.y, anchorB.x, anchorB.y]);
}

Chipmunk.prototype.cpConstraintSetUserData = function(constraint, userData)
{
    this.constraintsUserDatas.set(constraint, userData);
}

Chipmunk.prototype.cpConstraintGetUserData = function(constraint)
{
    return this.constraintsUserDatas.get(constraint);
}

Chipmunk.prototype.cpConstraintGetMaxBias = function(constraint)
{
    return this.runtime.ccall('cpConstraintGetMaxBias', 'number', ['number'], [constraint]);
}

Chipmunk.prototype.cpConstraintSetMaxBias = function(constraint, maxBias)
{
    return this.runtime.ccall('cpConstraintSetMaxBias', null, ['number', 'number'], [constraint, maxBias]);
}

Chipmunk.prototype.cpConstraintGetMaxForce = function(constraint)
{
    return this.runtime.ccall('cpConstraintGetMaxForce', 'number', ['number'], [constraint]);
}

Chipmunk.prototype.cpConstraintSetMaxForce = function(constraint, maxForce)
{
    return this.runtime.ccall('cpConstraintSetMaxForce', null, ['number', 'number'], [constraint, maxForce]);
}

Chipmunk.prototype.cpConstraintGetErrorBias = function(constraint)
{
    return this.runtime.ccall('cpConstraintGetErrorBias', 'number', ['number'], [constraint]);
}

Chipmunk.prototype.cpConstraintSetErrorBias = function(constraint, errorBias)
{
    return this.runtime.ccall('cpConstraintSetErrorBias', null, ['number', 'number'], [constraint, errorBias]);
}

Chipmunk.prototype.cpConstraintFree = function(constraint)
{
    this.runtime.ccall('cpConstraintFree', null, ['number'], [constraint]);
}

Chipmunk.prototype.cpArbiterGetShapes = function(arbiter)
{
    var shapes = { shapeA: null, shapeB: null };
    var stack = this.runtime.stackSave();
    var shapesPtr = this.runtime.allocate([shapes.shapeA, shapes.shapeB], '*', this.runtime.ALLOC_STACK);
    this.runtime.ccall('chipmunk_cpArbiterGetShapes', null, ['number', 'number'], [arbiter, shapesPtr]);
    shapes.shapeA = this.runtime.getValue(shapesPtr + 0, '*');
    shapes.shapeB = this.runtime.getValue(shapesPtr + 4, '*');
    this.runtime.stackRestore(stack);
    return shapes;
}

Chipmunk.prototype.cpArbiterGetBodies = function(arbiter)
{
    var bodies = { bodyA: null, bodyB: null };
    var stack = this.runtime.stackSave();
    var bodiesPtr = this.runtime.allocate([bodies.bodyA, bodies.bodyB], '*', this.runtime.ALLOC_STACK);
    this.runtime.ccall('chipmunk_cpArbiterGetBodies', null, ['number', 'number'], [arbiter, bodiesPtr]);
    bodies.bodyA = this.runtime.getValue(bodiesPtr + 0, '*');
    bodies.bodyB = this.runtime.getValue(bodiesPtr + 4, '*');
    this.runtime.stackRestore(stack);
    return bodies;
}

Chipmunk.prototype.cpArbiterGetNormal = function(arbiter)
{
    var normal = cpvlib.cpv(0.0, 0.0);
    var stack = this.runtime.stackSave();
    var normalPtr = this.runtime.allocate([normal.x, normal.y], 'double', this.runtime.ALLOC_STACK);
    this.runtime.ccall('chipmunk_cpArbiterGetNormal', null, ['number', 'number'], [body, normalPtr]);
    normal.x = this.runtime.getValue(normalPtr + 0, 'double');
    normal.y = this.runtime.getValue(normalPtr + 8, 'double');
    this.runtime.stackRestore(stack);
    return normal;
}

Chipmunk.prototype.cpArbiterIgnore = function(arbiter)
{    
    return this.runtime.ccall('cpArbiterIgnore', 'boolean', ['number'], [arbiter]);
}

Chipmunk.prototype.cpArbiterSetElasticity = function(arbiter, elasticity)
{
    return this.runtime.ccall('cpArbiterSetRestitution', null, ['number', 'number'], [arbiter, elasticity]);
}

Chipmunk.prototype.cpArbiterSetFriction = function(arbiter, friction)
{
    return this.runtime.ccall('cpArbiterSetFriction', null, ['number', 'number'], [arbiter, friction]);
}

Chipmunk.prototype.cpArbiterSetSurfaceVelocity = function(arbiter, surfaceVelocity)
{
    this.runtime.ccall('chipmunk_cpArbiterSetSurfaceVelocity', null, ['number', 'number', 'number'], [body, surfaceVelocity.x, surfaceVelocity.y]);
}

var chipmunk = new Chipmunk();

chipmunk.CP_VERSION_MAJOR = 7;
chipmunk.CP_VERSION_MINOR = 0;
chipmunk.CP_VERSION_RELEASE = 1;

chipmunk.CP_BODY_TYPE_DYNAMIC   = 0;
chipmunk.CP_BODY_TYPE_KINEMATIC = 1;
chipmunk.CP_BODY_TYPE_STATIC    = 2;

// DebugDrawModes
chipmunk.DebugDrawModes = {
    DBG_NoDebug: 0,
    DBG_DrawShapes: 1,
    DBG_DrawConstraints: 2,
    DBG_DrawCollisionPoints: 4
};

chipmunk.cpAreaForCircle = function cpAreaForCircle(r1, r2)
{
	return cpvlib.CP_PI * cpvlib.cpfabs(r1*r1 - r2*r2);
}

if (typeof global !== 'undefined') {
    global.cpVect = cpvlib.cpVect;
    global.cpShapeFilter = cpslib.cpShapeFilter;
}
else if (typeof window !== 'undefined') {
    window.cpVect = cpvlib.cpVect;
    window.cpShapeFilter = cpslib.cpShapeFilter;
}
else if (typeof self !== 'undefined') {
    self.cpVect = cpvlib.cpVect;
    self.cpShapeFilter = cpslib.cpShapeFilter;    
}

exports.chipmunk = chipmunk;
exports.webassembly = libchipmunk;
exports.cpvlib = cpvlib;
exports.cpslib = cpslib;
