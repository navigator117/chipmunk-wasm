var express = require('express');

var app = express();
app.use(express.static('./'));
app.use(express.static('../Chipmunk2D/'));

process.on('uncaughtException', function (error) {
    console.log('caught exception: ' + error);
});

var server = app.listen(8888, "0.0.0.0", function() {
    var host = server.address().address;
    var port = server.address().port;
    console.log('test listening at http://%s:%s', host, port);
});
